package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

	LocalDate currentDate = LocalDate.of(2022, 2, 3);
	ArrayList<FridgeItem> items;
	final int maxCapacity;
	
	public Fridge() {
		items = new ArrayList<FridgeItem>();
		maxCapacity = 20;
	}
	
	@Override
	public int nItemsInFridge() {
		return this.items.size();
	}

	@Override
	public int totalSize() {
		return this.maxCapacity;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (this.items.size() < maxCapacity) {
			this.items.add(item);
		}
		for(int i = 0; i<this.items.size(); i++) {
			if(this.items.get(i) == item) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(this.items.isEmpty()) {
			throw new NoSuchElementException("Fridge is empty");
		}
		this.items.remove(item);
	}

	@Override
	public void emptyFridge() {
		this.items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredItems = new ArrayList<>();
		for(int i = 0; i<this.items.size(); i++) {
			if(this.items.get(i).getExpirationDate().compareTo(currentDate) < 0) {
				expiredItems.add(this.items.get(i));
				this.items.remove(this.items.get(i));
				i--;
			}
		}
		System.out.println("Test");
		return expiredItems;
	}

}
